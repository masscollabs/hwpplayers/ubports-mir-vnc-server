#!/bin/bash

set -Eeou pipefail

cd "${ROOT}"
make -j$(( ( $(getconf _NPROCESSORS_ONLN) + 1) / 2 ))

cat << EOF >> vncserver.sh
#!/bin/sh
SCRIPTPATH="\$(dirname \$(realpath -s \$0))"
export LD_LIBRARY_PATH="\$SCRIPTPATH/lib/${ARCH_TRIPLET}/"

\$SCRIPTPATH/mirvncserver -m /run/mir_socket "\$@"
EOF

chmod +x vncserver.sh
