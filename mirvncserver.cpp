/*
 * Copyright © 2014 Canonical Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexandros Frantzis <alexandros.frantzis@canonical.com>
 */

#include "mir_toolkit/mir_client_library.h"
#include "mir_toolkit/mir_screencast.h"
#include "mir/geometry/size.h"
#include "mir/geometry/rectangle.h"

#include "mir/raii.h"

#include <boost/program_options.hpp>

#include <getopt.h>
#include <csignal>

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

#include <fstream>
#include <sstream>
#include <thread>
#include <iostream>
#include <stdexcept>
#include <future>
#include <vector>
#include <utility>
#include <chrono>

#include <unistd.h>
#include <rfb/rfb.h>
#include <rfb/keysym.h>

#include <stdio.h>
#include <libevdev/libevdev.h>
#include <libevdev/libevdev-uinput.h>

#include <fcntl.h>    /* For O_RDWR */
#include <unistd.h>   /* For open(), creat() */


namespace po = boost::program_options;

namespace
{

volatile sig_atomic_t running = 1;

/* On devices with android based openGL drivers, the vendor dispatcher table
 * may be optimized if USE_FAST_TLS_KEY is set, which hardcodes a TLS slot where
 * the vendor opengl function pointers live. Since glibc is not aware of this
 * use, collisions may happen.
 * Allocating a thread_local array helps avoid collisions by any thread_local usage
 * in async/future implementations.
 */
thread_local int dummy_tls[2];

struct libevdev *dev;
struct libevdev_uinput *uidev;

int maxx, maxy;
bool landscape = false;
bool mirror = false;
float scale_factor;
int orientation;

bool lmb;
bool rmb;
bool mmb;


void shutdown(int)
{
    running = 0;
}

void read_pixels(int bpp, GLenum format, mir::geometry::Size const& size, std::vector<char> &fb)
{
    auto width = size.width.as_uint32_t();
    auto height = size.height.as_uint32_t();

    // https://stackoverflow.com/a/60696921
    std::vector<char> pixels(bpp * width * height);
    glReadPixels(0, 0, width, height, format, GL_UNSIGNED_BYTE, pixels.data());

    // Mirror image
    if (mirror) {
        std::vector<char> mirrored(bpp * width * height);
        int i = 0;
        for (int y = height; y > 0; y--) {
            for (int x = width*bpp; x > 0; x -= bpp) {
	        int offset = (width * bpp * y) + x;

	        for (int p = 0; p < bpp; p++)
	            mirrored[i+p] = pixels[offset-bpp+p];

	        i += bpp;
           }
        }
	pixels = mirrored;
    }

    // Rotate to landscape
    if (landscape) {
	/** Starts from the last column and moves back reading each vertically and writing it horizontally.
	 * Given the following matrix of pixels (R G B):
	 * (1 1 1) (4 4 4) (6 6 6)
	 * (3 3 3) (2 2 2) (5 5 5)
	 * (7 7 7) (9 9 9) (8 8 8)
	 *
	 * It will be rotated 90 degrees in either direction, with the pixel values remaining grouped, e.g.:
	 *
	 * (90 degrees anti-clockwise)
	 * (6 6 6) (5 5 5) (8 8 8)
	 * (4 4 4) (2 2 2) (9 9 9)
	 * (1 1 1) (3 3 3) (7 7 7)
	 *
	 **/
        int i = 0;
        for (int x = 0; x < width*bpp; x += bpp) {
	    for (int y = height; y > 0; y--) {
	        int offset = (width * bpp * y) + x;

	        // Set each of <bpp> pixels (i.e. <offset-3>, <offset-2>, <offset-1> <offset>)
	        for (int p = 0; p < bpp; p++)
	            fb[i+p] = pixels[offset-bpp+p];

	        i += bpp;
	    }
        }
    }
    else
	fb = pixels;
#ifdef DEBUG
    unsigned int sum = 0;
    unsigned int *pui = (unsigned int *) buffer;
    for (int i = 0; i < width * height; i++) {
        sum += *(pui + i);
    }
    printf("read pixels sum 0x%x\n", sum);
#endif
}


uint32_t get_first_valid_output_id(MirDisplayConfiguration const& display_config)
{
    for (unsigned int i = 0; i < display_config.num_outputs; ++i)
    {
        MirDisplayOutput const& output = display_config.outputs[i];

        if (output.connected && output.used &&
            output.current_mode < output.num_modes)
        {
            return output.output_id;
        }
    }

    throw std::runtime_error("Couldn't find a valid output to screencast");
}

MirRectangle get_screen_region_from(MirDisplayConfiguration const& display_config, uint32_t output_id)
{
    if (output_id == mir_display_output_id_invalid)
        output_id = get_first_valid_output_id(display_config);

    for (unsigned int i = 0; i < display_config.num_outputs; ++i)
    {
        MirDisplayOutput const& output = display_config.outputs[i];

        if (output.output_id == output_id &&
            output.current_mode < output.num_modes)
        {
            MirDisplayMode const& mode = output.modes[output.current_mode];
            return MirRectangle{output.position_x, output.position_y,
                                mode.horizontal_resolution,
                                mode.vertical_resolution};
        }
    }

    throw std::runtime_error("Couldn't get screen region of specified output");
}

MirScreencastParameters get_screencast_params(MirConnection* connection,
                                              MirDisplayConfiguration const& display_config,
                                              std::vector<int> const& requested_size,
                                              std::vector<int> const& requested_region,
                                              uint32_t output_id)
{
    MirScreencastParameters params;

    if (requested_region.size() == 4)
    {
        /* TODO: the region should probably be validated
         * to make sure it overlaps any one of the active display areas
         */
        params.region.left = requested_region[0];
        params.region.top = requested_region[1];
        params.region.width = requested_region[2];
        params.region.height = requested_region[3];
    }
    else
    {
        params.region = get_screen_region_from(display_config, output_id);
    }

    if (requested_size.size() == 2)
    {
        params.width = requested_size[0];
        params.height = requested_size[1];
    }
    else
    {
        params.width = params.region.width;
        params.height = params.region.height;
    }

    unsigned int num_valid_formats = 0;
    mir_connection_get_available_surface_formats(connection, &params.pixel_format, 1, &num_valid_formats);

    if (num_valid_formats == 0)
        throw std::runtime_error("Couldn't find a valid pixel format for connection");

    return params;
}

double get_capture_rate_limit(MirDisplayConfiguration const& display_config, MirScreencastParameters const& params)
{
    mir::geometry::Rectangle screencast_area{
        {params.region.left, params.region.top},
        {params.region.width, params.region.height}};

    double highest_refresh_rate = 0.0;
    for (unsigned int i = 0; i < display_config.num_outputs; ++i)
    {
        MirDisplayOutput const& output = display_config.outputs[i];
        if (output.connected && output.used &&
            output.current_mode < output.num_modes)
        {
            MirDisplayMode const& mode = output.modes[output.current_mode];
            mir::geometry::Rectangle display_area{
                {output.position_x, output.position_y},
                {mode.horizontal_resolution, mode.vertical_resolution}};
            if (display_area.overlaps(screencast_area) && mode.refresh_rate > highest_refresh_rate)
                highest_refresh_rate = mode.refresh_rate;
        }
    }

    /* Either the display config has invalid refresh rate data
     * or the screencast region is outside of any display area, either way
     * let's default to sensible max capture limit
     */
    if (highest_refresh_rate == 0.0)
        highest_refresh_rate = 60.0;

    return highest_refresh_rate;
}

struct EGLSetup
{
    EGLSetup(MirConnection* connection, MirScreencast* screencast)
    {
        static EGLint const attribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_RED_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_BLUE_SIZE, 8,
            EGL_ALPHA_SIZE, 8,
            EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_NONE};

        static EGLint const context_attribs[] = {
            EGL_CONTEXT_CLIENT_VERSION, 2,
            EGL_NONE };

        auto native_display =
            reinterpret_cast<EGLNativeDisplayType>(
                mir_connection_get_egl_native_display(connection));

	MirBufferStream *buffer_stream_;
	buffer_stream_ = mir_screencast_get_buffer_stream(screencast);
        auto native_window =
            reinterpret_cast<EGLNativeWindowType>(
                mir_buffer_stream_get_egl_native_window(buffer_stream_)
	    );

        egl_display = eglGetDisplay(native_display);

        eglInitialize(egl_display, nullptr, nullptr);

        int n;
        eglChooseConfig(egl_display, attribs, &egl_config, 1, &n);

        egl_surface = eglCreateWindowSurface(egl_display, egl_config, native_window, NULL);
        if (egl_surface == EGL_NO_SURFACE)
            throw std::runtime_error("Failed to create EGL screencast surface");

        egl_context = eglCreateContext(egl_display, egl_config, EGL_NO_CONTEXT, context_attribs);
        if (egl_context == EGL_NO_CONTEXT)
            throw std::runtime_error("Failed to create EGL context for screencast");

        if (eglMakeCurrent(egl_display, egl_surface,
                           egl_surface, egl_context) != EGL_TRUE)
        {
            throw std::runtime_error("Failed to make screencast surface current");
        }

        uint32_t a_pixel;
        glReadPixels(0, 0, 1, 1, GL_BGRA_EXT, GL_UNSIGNED_BYTE, &a_pixel);
        if (glGetError() == GL_NO_ERROR)
            read_pixel_format = GL_BGRA_EXT;
        else
            read_pixel_format = GL_RGBA;
    }

    ~EGLSetup()
    {
        eglMakeCurrent(egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroySurface(egl_display, egl_surface);
        eglDestroyContext(egl_display, egl_context);
        eglTerminate(egl_display);
    }

    void swap_buffers() const
    {
        if (eglSwapBuffers(egl_display, egl_surface) != EGL_TRUE)
            throw std::runtime_error("Failed to swap screencast surface buffers");
    }

    GLenum pixel_read_format() const
    {
        return read_pixel_format;
    }

    EGLDisplay egl_display;
    EGLContext egl_context;
    EGLSurface egl_surface;
    EGLConfig egl_config;
    GLenum read_pixel_format;
};


// https://github.com/hanzelpeter/dispmanx_vnc/blob/master/main.c
static int keysym2scancode(rfbKeySym key)
{
	int scancode = 0;

	int code = (int) key;
	if (code>='0' && code<='9') {
		scancode = (code & 0xF) - 1;
		if (scancode<0) scancode += 10;
		scancode += KEY_1;
	} else if (code>=0xFF50 && code<=0xFF58) {
		static const uint16_t map[] =
		{  KEY_HOME, KEY_LEFT, KEY_UP, KEY_RIGHT, KEY_DOWN,
		KEY_PAGEUP, KEY_PAGEDOWN, KEY_END, 0 };
		scancode = map[code & 0xF];
	} else if (code>=0xFFE1 && code<=0xFFEE) {
		static const uint16_t map[] =
		{  KEY_LEFTSHIFT, KEY_LEFTSHIFT,
		KEY_RIGHTSHIFT, KEY_LEFTCTRL,
		KEY_RIGHTCTRL, KEY_LEFTSHIFT,
		0,0,
		KEY_LEFTALT, KEY_RIGHTALT,
		0, 0, 0, 0 };
		scancode = map[code & 0xF];
	} else if ((code>='A' && code<='Z') || (code>='a' && code<='z')) {
		static const uint16_t map[] = {
			KEY_A, KEY_B, KEY_C, KEY_D, KEY_E,
			KEY_F, KEY_G, KEY_H, KEY_I, KEY_J,
			KEY_K, KEY_L, KEY_M, KEY_N, KEY_O,
			KEY_P, KEY_Q, KEY_R, KEY_S, KEY_T,
			KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z };
		scancode = map[(code & 0x5F) - 'A'];
	} else {
		switch (code) {
		case XK_space:    scancode = KEY_SPACE;       break;

		case XK_exclam: scancode = KEY_1; break;
		case XK_at:     scancode         = KEY_2; break;
		case XK_numbersign:    scancode      = KEY_3; break;
		case XK_dollar:    scancode  = KEY_4; break;
		case XK_percent:    scancode = KEY_5; break;
		case XK_asciicircum:    scancode     = KEY_6; break;
		case XK_ampersand:    scancode       = KEY_7; break;
		case XK_asterisk:    scancode        = KEY_8; break;
		case XK_parenleft:    scancode       = KEY_9; break;
		case XK_parenright:    scancode      = KEY_0; break;
		case XK_minus:    scancode   = KEY_MINUS; break;
		case XK_underscore:    scancode      = KEY_MINUS; break;
		case XK_equal:    scancode   = KEY_EQUAL; break;
		case XK_plus:    scancode    = KEY_EQUAL; break;
		case XK_BackSpace:    scancode       = KEY_BACKSPACE; break;
		case XK_Tab:    scancode             = KEY_TAB; break;

		case XK_braceleft:    scancode        = KEY_LEFTBRACE;     break;
		case XK_braceright:    scancode       = KEY_RIGHTBRACE;     break;
		case XK_bracketleft:    scancode      = KEY_LEFTBRACE;     break;
		case XK_bracketright:    scancode     = KEY_RIGHTBRACE;     break;
		case XK_Return:    scancode  = KEY_ENTER;     break;

		case XK_semicolon:    scancode        = KEY_SEMICOLON;     break;
		case XK_colon:    scancode    = KEY_SEMICOLON;     break;
		case XK_apostrophe:    scancode       = KEY_APOSTROPHE;     break;
		case XK_quotedbl:    scancode         = KEY_APOSTROPHE;     break;
		case XK_grave:    scancode    = KEY_GRAVE;     break;
		case XK_asciitilde:    scancode       = KEY_GRAVE;     break;
		case XK_backslash:    scancode        = KEY_BACKSLASH;     break;
		case XK_bar:    scancode              = KEY_BACKSLASH;     break;

		case XK_comma:    scancode    = KEY_COMMA;      break;
		case XK_less:    scancode     = KEY_COMMA;      break;
		case XK_period:    scancode   = KEY_DOT;      break;
		case XK_greater:    scancode  = KEY_DOT;      break;
		case XK_slash:    scancode    = KEY_SLASH;      break;
		case XK_question:    scancode         = KEY_SLASH;      break;
		case XK_Caps_Lock:    scancode        = KEY_CAPSLOCK;      break;

		case XK_F1:    scancode               = KEY_F1; break;
		case XK_F2:    scancode               = KEY_F2; break;
		case XK_F3:    scancode               = KEY_F3; break;
		case XK_F4:    scancode               = KEY_F4; break;
		case XK_F5:    scancode               = KEY_F5; break;
		case XK_F6:    scancode               = KEY_F6; break;
		case XK_F7:    scancode               = KEY_F7; break;
		case XK_F8:    scancode               = KEY_F8; break;
		case XK_F9:    scancode               = KEY_F9; break;
		case XK_F10:    scancode              = KEY_F10; break;
		case XK_Num_Lock:    scancode         = KEY_NUMLOCK; break;
		case XK_Scroll_Lock:    scancode      = KEY_SCROLLLOCK; break;

		case XK_Page_Down:    scancode        = KEY_PAGEDOWN; break;
		case XK_Insert:    scancode   = KEY_INSERT; break;
		case XK_Delete:    scancode   = KEY_DELETE; break;
		case XK_Page_Up:    scancode  = KEY_PAGEUP; break;
		case XK_Escape:    scancode   = KEY_ESC; break;

		case XK_KP_Divide:   scancode = KEY_KPSLASH; break;
		case XK_KP_Multiply: scancode = KEY_KPASTERISK; break;
		case XK_KP_Add:      scancode = KEY_KPPLUS; break;
		case XK_KP_Subtract: scancode = KEY_KPMINUS; break;
		case XK_KP_Enter:    scancode = KEY_KPENTER; break;

		case XK_KP_Decimal:
		case XK_KP_Delete:
			scancode = KEY_KPDOT; break;

		case XK_KP_0:
		case XK_KP_Insert:
			scancode = KEY_KP0; break;

		case XK_KP_1:
		case XK_KP_End:
			scancode = KEY_KP1; break;

		case XK_KP_2:
		case XK_KP_Down:
			scancode = KEY_KP2; break;

		case XK_KP_3:
		case XK_KP_Page_Down:
			scancode = KEY_KP3; break;

		case XK_KP_4:
		case XK_KP_Left:
			scancode = KEY_KP4; break;

		case XK_KP_5:
			scancode = KEY_KP5; break;

		case XK_KP_6:
		case XK_KP_Right:
			scancode = KEY_KP6; break;

		case XK_KP_7:
		case XK_KP_Home:
			scancode = KEY_KP7; break;

		case XK_KP_8:
		case XK_KP_Up:
			scancode = KEY_KP8; break;

		case XK_KP_9:
		case XK_KP_Page_Up:
			scancode = KEY_KP9; break;

		}
	}

	return scancode;
}


// Rotation logic
int mod(int a, int b)
{ return (a%b+b)%b; }

void rotate(int direction, rfbClientPtr cl) {
    orientation += direction;
    orientation = mod(orientation, 4);  // 4 orientations
    bool landscape_cp = landscape;
    switch (orientation) {
        case 0:
	    mirror = false;
	    landscape = false;
	    break;
	case 1:
	    mirror = false;
	    landscape = true;
	    break;
	case 2:
	    mirror = true;
	    landscape = false;
	    break;
	case 3:
	    mirror = true;
	    landscape = true;
	    break;
    }

    if (landscape != landscape_cp) { // Landscape mode toggled
        std::swap(maxx, maxy);
    }
}


// vnc key event function, escape to quit
static void dokey(rfbBool down,rfbKeySym key,rfbClientPtr cl)
{
  /* if(down && key==XK_Escape) { */
  /*     /1* close down server, disconnecting clients *1/ */
  /*     rfbShutdownServer(cl->screen,TRUE); */
  /*     running = 0; */
  /* } */
  // printf("keysym: %04X\n", key);

  if (down && key == 0xFF52)  // Ctrl+Super+Up
      rotate(1, cl);
  else if (down && key == 0xFF54)  // Ctrl+Super+Down
      rotate(-1, cl);
  else
      libevdev_uinput_write_event(uidev, EV_KEY, keysym2scancode(key), down);
      libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
}


static void doptr(int buttonMask, int x, int y, rfbClientPtr cl)
{
  int left_down = buttonMask & 0x1;
  int right_down = buttonMask & 0x4;
  int middle_down = buttonMask & 0x2;

  if (mirror) {
    y = maxy - y;
    x = maxx - x;
  }

  if (landscape) {
    x = maxx - x;
    std::swap(x, y);
  }

  if (left_down) {
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_LEFT, 1);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);

    libevdev_uinput_write_event(uidev, EV_ABS, ABS_MT_TRACKING_ID, 10);
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_TOUCH, 1);
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_TOOL_FINGER, 1);
    libevdev_uinput_write_event(uidev, EV_ABS, ABS_MT_POSITION_X, x*scale_factor);
    libevdev_uinput_write_event(uidev, EV_ABS, ABS_MT_POSITION_Y, y*scale_factor);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    lmb = true;
  }
  else if (lmb) {
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_LEFT, 0);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);

    libevdev_uinput_write_event(uidev, EV_ABS, ABS_MT_TRACKING_ID, -1);
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_TOUCH, 0);
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_TOOL_FINGER, 0);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    lmb = false;
  }

  if (right_down || rmb) {
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_RIGHT, right_down >> 2);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    rmb ^= true;
  }

  if (middle_down || mmb) {
    libevdev_uinput_write_event(uidev, EV_KEY, BTN_MIDDLE, middle_down >> 1);
    libevdev_uinput_write_event(uidev, EV_SYN, SYN_REPORT, 0);
    mmb ^= true;
  }
}


void do_screencast(EGLSetup const& egl_setup,
                   mir::geometry::Size const& size,
                   int32_t number_of_captures,
                   double capture_fps,
                   std::ostream& out_stream)
{
    static int const rgba_pixel_size{4};
    auto const frame_size_bytes = rgba_pixel_size *
                                  size.width.as_uint32_t() *
                                  size.height.as_uint32_t();

    std::vector<char> frame_data(frame_size_bytes, 0);
    auto format = egl_setup.pixel_read_format();

    auto capture_period = std::chrono::duration<double>(1.0/capture_fps);

    // vnc server
    int bpp = 4;
    maxx = size.width.as_uint32_t();
    maxy = size.height.as_uint32_t();

    rfbScreenInfoPtr rfbScreen = rfbGetScreen(NULL,NULL,maxx,maxy,8,3,bpp);
    rfbScreen->desktopName = "MIR Screen";
    // rfbScreen->frameBuffer = (char*)malloc(maxx*maxy*bpp);
    rfbScreen->frameBuffer = frame_data.data();
    rfbScreen->alwaysShared = TRUE;
    rfbScreen->kbdAddEvent = dokey;
    rfbScreen->ptrAddEvent = doptr;
    rfbInitServer(rfbScreen);
    rfbRunEventLoop(rfbScreen,-1,TRUE);
    fprintf(stderr, "Running vnc background loop...\n");

    while (running)
    {
        auto time_point = std::chrono::steady_clock::now() + capture_period;

        read_pixels(bpp, format, size, frame_data);

	if (maxx/maxy != rfbScreen->width/rfbScreen->height) {
	    printf("Ratios don't match\n");
	    printf("RFB: %dx%d\n",  rfbScreen->width, rfbScreen->height);
	    printf("Screen: %dx%d\n",  size.width, size.height);
	    std::swap(rfbScreen->width, rfbScreen->height);
	    printf("RFB: %dx%d\n\n",  rfbScreen->width, rfbScreen->height);
	    {
		rfbClientIteratorPtr iterator;
		rfbClientPtr cl;
		iterator = rfbGetClientIterator(rfbScreen);
		while ((cl = rfbClientIteratorNext(iterator)) != NULL)
			cl->newFBSizePending = 1;
	    }
	    rfbMarkRectAsModified(rfbScreen, 0, 0, rfbScreen->width, rfbScreen->height);
	}


        auto write_out_future = std::async(
                std::launch::async,
                [rfbScreen, maxx, maxy] {
                    if (running)
                        rfbMarkRectAsModified(rfbScreen, 0, 0, maxx, maxy);
                });

        egl_setup.swap_buffers();
        write_out_future.wait();

        std::this_thread::sleep_until(time_point);
    }

    // TODO: SEGFAULT with this on, need checking...
    // rfbScreenCleanup(rfbScreen);
}

}

int main(int argc, char* argv[])
try
{
    uint32_t output_id = mir_display_output_id_invalid;
    int number_of_captures = -1;
    std::string output_filename;
    std::string socket_filename;
    std::vector<int> screen_region;
    std::vector<int> requested_size;
    bool use_std_out = false;
    bool query_params_only = false;
    int capture_interval = 1;

    // Fake input device (https://stackoverflow.com/q/23092855)
    dev = libevdev_new();
    libevdev_set_name(dev, "mirvncserver input device");
    libevdev_set_id_vendor(dev, 0x1);
    libevdev_set_id_product(dev, 0x1);
    libevdev_set_id_version(dev, 0x1);
    libevdev_set_id_bustype(dev, BUS_USB);

    // (Absolute) pointer input (http://who-t.blogspot.com/2013/09/libevdev-accessing-and-modifying-devices.html)
    struct input_absinfo abs;
    abs.minimum = abs.resolution = abs.fuzz = abs.flat = 0;

    libevdev_enable_event_type(dev, EV_ABS);

    abs.maximum = 9;
    libevdev_enable_event_code(dev, EV_ABS, ABS_MT_SLOT, &abs);

    abs.maximum = 65535;
    libevdev_enable_event_code(dev, EV_ABS, ABS_MT_TRACKING_ID, &abs);

    abs.maximum = 1080;
    libevdev_enable_event_code(dev, EV_ABS, ABS_X, &abs);
    libevdev_enable_event_code(dev, EV_ABS, ABS_MT_POSITION_X, &abs);

    abs.maximum = 1920;
    libevdev_enable_event_code(dev, EV_ABS, ABS_Y, &abs);
    libevdev_enable_event_code(dev, EV_ABS, ABS_MT_POSITION_Y, &abs);

    // Keyboard input
    libevdev_enable_event_type(dev, EV_KEY);
    libevdev_enable_event_code(dev, EV_KEY, BTN_LEFT, NULL);
    libevdev_enable_event_code(dev, EV_KEY, BTN_RIGHT, NULL);
    libevdev_enable_event_code(dev, EV_KEY, BTN_MIDDLE, NULL);
    libevdev_enable_event_code(dev, EV_KEY, BTN_TOUCH, NULL);
    libevdev_enable_event_code(dev, EV_KEY, BTN_TOOL_FINGER, NULL);
    for (int i = 0; i < KEY_MAX; i++)
      libevdev_enable_event_code(dev, EV_KEY, i, NULL);

    libevdev_uinput_create_from_device(dev,
        LIBEVDEV_UINPUT_OPEN_MANAGED,
        &uidev);

    //avoid unused warning/error
    dummy_tls[0] = 0;

    po::options_description desc("Usage");
    desc.add_options()
        ("help,h", "displays this message")
        ("number-of-frames,n",
            po::value<int>(&number_of_captures), "number of frames to capture")
        ("display-id,d",
            po::value<uint32_t>(&output_id), "id of the display to capture")
        ("mir-socket-file,m",
            po::value<std::string>(&socket_filename), "mir server socket filename")
        ("file,f",
            po::value<std::string>(&output_filename), "output filename (default is /tmp/mir_screencast_<w>x<h>.<rgba|bgra>")
        ("size,s",
            po::value<std::vector<int>>(&requested_size)->multitoken(),
            "screencast size [width height]")
        ("screen-region,r",
            po::value<std::vector<int>>(&screen_region)->multitoken(),
            "screen region to capture [left top width height]")
        ("stdout", po::value<bool>(&use_std_out)->zero_tokens(), "use stdout for output (--file is ignored)")
        ("query",
            po::value<bool>(&query_params_only)->zero_tokens(),
            "only queries the colorspace and output size used but does not start screencast")
        ("cap-interval",
            po::value<int>(&capture_interval),
            "adjusts the capture rate to <arg> display refresh intervals\n"
            "1 -> capture at display rate\n2 -> capture at half the display rate, etc..");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("size") && requested_size.size() != 2)
            throw po::error("invalid number of parameters specified for size");

        if (vm.count("screen-region") && screen_region.size() != 4)
            throw po::error("invalid number of parameters specified for screen-region");

        if (vm.count("display-id") && vm.count("screen-region"))
            throw po::error("cannot set both display-id and screen-region");

        if (vm.count("cap-interval") && capture_interval < 1)
            throw po::error("invalid capture interval");
    }
    catch(po::error& e)
    {
        std::cerr << e.what() << std::endl << std::endl;
        std::cerr << desc << std::endl;
        return EXIT_FAILURE;
    }

    if (vm.count("help"))
    {
        std::cout << desc << std::endl;
        return EXIT_SUCCESS;
    }

    signal(SIGINT, shutdown);
    signal(SIGTERM, shutdown);

    char const* socket_name = vm.count("mir-socket-file") ? socket_filename.c_str() : nullptr;
    auto const connection = mir::raii::deleter_for(
        mir_connect_sync(socket_name, "mirscreencast"),
        [](MirConnection* c) { if (c) mir_connection_release(c); });

    if (connection == nullptr || !mir_connection_is_valid(connection.get()))
    {
        std::string msg("Failed to connect to server.");
        if (connection)
        {
            msg += " Error was :";
            msg += mir_connection_get_error_message(connection.get());
        }
        throw std::runtime_error(std::string(msg));
    }

    auto const display_config = mir::raii::deleter_for(
        mir_connection_create_display_config(connection.get()),
        &mir_display_config_destroy);

    if (display_config == nullptr)
        throw std::runtime_error("Failed to get display configuration\n");

    MirScreencastParameters params =
        get_screencast_params(connection.get(), *display_config, requested_size, screen_region, output_id);

    // Calculate scale factor (NOTE: assumes scale ratio is equivalent on both axes)
    scale_factor = (float)params.region.width / (float)params.width;
    printf("%d %d %f\n", params.width, params.region.width, scale_factor);

    double capture_rate_limit = get_capture_rate_limit(*display_config, params);
    double capture_fps = capture_rate_limit/capture_interval;

    auto const screencast = mir::raii::deleter_for(
        mir_connection_create_screencast_sync(connection.get(), &params),
        [](MirScreencast* s) { if (s) mir_screencast_release_sync(s); });

    if (screencast == nullptr)
        throw std::runtime_error("Failed to create screencast");

    EGLSetup egl_setup{connection.get(), screencast.get()};
    mir::geometry::Size screencast_size {params.width, params.height};

    if (output_filename.empty() && !use_std_out)
    {
        std::stringstream ss;
        ss << "/tmp/mir_screencast_" ;
        ss << screencast_size.width << "x" << screencast_size.height;
        ss << "_" << capture_fps << "Hz";
        ss << (egl_setup.pixel_read_format() == GL_BGRA_EXT ? ".bgra" : ".rgba");
        output_filename = ss.str();
    }

    if (query_params_only)
    {
       std::cout << "Colorspace: " <<
           (egl_setup.pixel_read_format() == GL_BGRA_EXT ? "BGRA" : "RGBA") << std::endl;
       std::cout << "Output size: " <<
           screencast_size.width << "x" << screencast_size.height << std::endl;
       std::cout << "Capture rate (Hz): " << capture_fps << std::endl;
       std::cout << "Output to: " <<
           (use_std_out ? "standard out" : output_filename) << std::endl;
       return EXIT_SUCCESS;
    }

    if (use_std_out)
    {
        do_screencast(egl_setup, screencast_size, number_of_captures, capture_fps, std::cout);
    }
    else
    {
        std::ofstream file_stream(output_filename);
        do_screencast(egl_setup, screencast_size, number_of_captures, capture_fps, file_stream);
    }

    // Clear libevdev devices
    libevdev_uinput_destroy(uidev);
    libevdev_free(dev);

    return EXIT_SUCCESS;
}
catch(std::exception const& e)
{
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
}
